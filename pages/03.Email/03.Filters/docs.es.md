---
title: Filtros de correo
published: true
visible: true
taxonomy:
    category:
        - docs
---

Los filtros de correo te permiten administrar los correos entrantes de manera tal que ingresen a una carpeta específica, basándose en ciertos criterios (p.ej: trabajo/personal/proyectos, etc.); establecer respuestas automáticas (p.ej: fuera de la oficina/de vacaciones, etc); rechazar o reenviar automáticamente correos, etc.

En esta sección, cubriremos lo elemental, en algunos escenarios.
