---
title: Email Filters
published: true
visible: true
taxonomy:
    category:
        - docs
---

Email Filters allow you to manage incoming mails in automated fashion such as moving incoming mail to a directory based on certain criteria, setting up out-of-office/holiday auotreply, automatically reject or forward emails etc.

In this section we will cover the basics based on some scenarios.
