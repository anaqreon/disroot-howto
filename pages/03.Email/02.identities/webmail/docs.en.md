---
title: Setup Mail Alias on webmail
published: true
visible: false
taxonomy:
    category:
        - docs
---

# Setup
First of, login to your webmail and go to your mail settings (bottom left icon)

![](en/settings1.png)

When in Settings, go to **"Identities"** tab, click "**Add an Identity"** and fillin the form. Once done, hit **"Add"** button.
*(Every disroot user by default has an username@disr.it alias he can use)*
![](en/identity_add.gif)

# Set default
You can manage default identity, by simply dragging the identity to the top of the list.
![](en/identity_default.gif)

# Send email
To send email with your new alias, just click on the **"From"** field and select alias you want to use from the dropdown menu, when composing your mail.
![](en/identity_send.gif)
