---
title: Configurar um alias de email no Thunderbird
published: true
visible: false
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

# Configurações
Primeiro, inicie o Thunderbird e vá a "Definições" carregando com a tecla direita do rato na sua conta de email.
![](pt/identity_settings.gif)

No canto inferior direito da janela de configurações da sua conta existe o botão **"Gerir Identidades..."**

Uma vez no painel de gestão de identidades, pode adicionar novos alias carregando em **"Adicionar..."** e preenchendo a informação no formulário e carregando em OK no fim:
![](pt/identity_add.gif)

# Set default
Se quiser colocar o seu novo alias de email como a identidade predefinida do seu email, selecione o alias de emails e carregue no botão **"Predefinição"**.
![](pt/identity_default.gif)

# Enviar email
Para enviar um email com o seu novo alias carregar no campo **"De:"** e escolher o alias que quer utilizar quando está a compor o seu email.
![](pt/identity_send.gif)
