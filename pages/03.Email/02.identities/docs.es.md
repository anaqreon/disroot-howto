---
title: Cómo configurar el Alias de Correo
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Una vez que hayas solicitado los alias utilizando el [formulario](https://disroot.org/forms/alias-request-form), necesitarás configurarlos. Aquí debajo encontrarás cómo hacerlo en varios clientes de correo.

## Índice
- [Webmail](webmail)
- [Thunderbird](thunderbird)
- [K9](k9)
