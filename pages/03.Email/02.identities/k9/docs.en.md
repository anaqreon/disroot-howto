---
title: Setup Mail Alias on your Android phone (K9)
published: true
visible: false
taxonomy:
    category:
        - docs
---

# Setup
First of, open K9 and go to your accounts settings
![](en/identity_settings.png)

When in Settings, go to **"Sending Mail"** tab, tap on **"Manage Identities"**.

![](en/identity_settings2.png)

Select **"New identity"** by tapping the "three dot" icon on top right.
*(Every disroot user by default has an username@disr.it alias he can use)*
![](en/identity_settings3.png)

And fill in the form providing the new alias address.
![](en/identity_settings4.png)

# Set default
To change the default identity, while still in **"Manage Identities"** settings, just tap and hold the alias you want to set and select **"Move to top / make default"** option.
![](en/identity_settings5.png)

# Send email
To send email with your new alias, just tap on the **"Form"** field and select alias you want to use from the dropdown menu, when composing your mail.
