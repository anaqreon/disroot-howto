---
title: How to setup mail alias
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Once you requested email aliases using the [form](https://disroot.org/forms/alias-request-form). You need to set them up. Below you can find how to do it on various email clients.

## Table of contents
- [Webmail](webmail)
- [Thunderbird](thunderbird)
- [K9](k9)
