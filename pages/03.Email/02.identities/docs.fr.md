---
title: Comment configurer l'alias de messagerie
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Une fois que vous avez demandé des alias de messagerie en utilisant le [formulaire](https://disroot.org/forms/alias-request-form), vous devez les mettre en place. Ci-dessous vous trouverez comment le faire sur différents clients de messagerie.

Table des matières
- [Webmail](webmail)
- [Thunderbird](thunderbird)
- [K9](k9)
