---
title: Cómo configurar clientes de correo electrónico
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Esta sección está destinada a ayudarte a abandonar el webmail e introducirte en una experiencia verdaderamente productiva y nativa en tu dispositivo.

Si tu cliente de correo no aparece listado, sólo sigue la configuración general del servidor para ponerlo en marcha. Configurar clientes de correo es bastante claro y sencillo (a menos que utilices Outlook, pero a quién le importa eso :))

```
Servidor IMAP: disroot.org | Puerto SSL 993 | Método de Autenticación: Contraseña Normal
Servidor SMTP: disroot.org | Puerto STARTTLS 587 | Método de Autenticación: Contraseña Normal
Servidor POP:  disroot.org | Puerto SSL 995 | Método de Autenticación: Contraseña Normal
```
En caso que quisieras ver un tutorial acerca de cómo configurar un cliente de correo de tu preferencia que no esté listado aún, considera la posibilidad de escribir tú mismo un breve manual y compartirlo con la comunidad.
