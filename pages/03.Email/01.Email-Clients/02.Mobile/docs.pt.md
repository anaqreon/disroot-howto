---
title: 'Clientes de telemóvel'
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Como configurar o seu email Disroot no seu telemóvel:

## Table of content
 - [K9 - Aplicação de email para Android](androidk9)
 - [SailfishOS](sailfishos)
 - [iOS](ios)

![](mobile.jpg)
