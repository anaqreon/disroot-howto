---
title: 'Clients de téléphone'
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Comment configurer votre email avec votre appareil mobile:

## Sommaire
 - [K9 - Une application email Android](androidk9)
 - [SailfishOS](sailfishos)
 - [iOS](ios)

![](mobile.jpg)
