---
title: 'Mobile Clients'
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

How to setup your email with your mobile device:

## Table of content
 - [K9 - Android Email App](androidk9)
 - [SailfishOS](sailfishos)
 - [iOS](ios)

![](mobile.jpg)
