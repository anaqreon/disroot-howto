---
title: 'Clientes de correo'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

Cómo configurar tu correo en el escritorio:

## Índice
- [Thunderbird - Cliente de correo multiplataforma](thunderbird)

![](c64.jpg)
