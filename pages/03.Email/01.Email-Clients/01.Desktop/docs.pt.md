---
title: 'Cliente de desktop'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Como configurar o seu cliente de email no seu desktop:

## Table of content
- [Thunderbird - cliente de email multiplataforma](thunderbird)

![](c64.jpg)
