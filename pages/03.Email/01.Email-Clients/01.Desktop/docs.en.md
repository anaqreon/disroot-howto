---
title: 'Desktop clients'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

How to setup your email on your desktop:

## Table of content
- [Thunderbird - multiplatform email client](thunderbird)

![](c64.jpg)
