---
title: How to setup Email Clients
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

This section is dedicated to get you off the stupid webmail crap and onto truly productive, native experience on your device.

If your favorite mail client isn't listed, just follow the general server settings to get up and running. Setting mail clients is pretty straight forward and easy (unless using outlook but who cares about that:)

```
IMAP: disroot.org | SSL Port 993 | Authentication: Normal Password
SMTP: disroot.org | STARTTLS Port 587 | Authentication: Normal Password
POP: disroot.org | SSL Port 995 | Authentication: Normal Password
```
In case you would like to see a tutorial on setting up client of your choice that isn't listed yet, consider writing up a short howto and share it with the community.
