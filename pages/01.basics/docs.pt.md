---
title: Noções Básicas
published: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

# Bem-vindos à página de manuais do **Disroot**.

Este site é dedicado a tutoriais e manuais para lhe ajudar a orientar-se com os vários serviços fornecidos pelo Disroot.

A nossa ideia é cobrir todos os serviços fornecidos pelo Disroot e todas as suas funcionalidades, para todas as plataformas e sistemas operativos. É um projeto bastante ambicioso que requer bastante trabalho. No entanto, nós acreditamos que beneficiará não apenas os disrooters, mas também toda a Comunidade de Open Source que utiliza o mesmo software ou semelhante.

De modo a poder cobrir todos os aspetos de cada serviço e mantê-los constantemente atualizados, necessitamos da ajuda de todos os disrooters. Por isso se acha que lhe falta um tutorial, ou que a informação não é correta, esteja à vontade para nos contactar ou escrever uma revisão tu próprio. Como deve ter reparado nós utilizamos o **git**, por isso basta fazer um pull request do nosso repositório.
Se não está familiarizado com o git, pode submeter pedidos de alterações via pad ou e-mail. Em breve vamos fazer um tutorial simples sobre como utilizar o git (ou pelo menos um link para um) para meter a bordo todas as pessoas interessadas.

![](en/disroot_logo.png)
