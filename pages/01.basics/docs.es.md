---
title: Lo básico
published: true
taxonomy:
    category:
        - docs
page-toc:
     active: false  
---

# Bienvenidos a los Manuales de **Disroot**.

Este sitio está dedicado a manuales y tutoriales para orientarte a través de los varios servicios de Disroot.

Nuestra idea es cubrir todos los servicios provistos por Disroot, con sus características, para todas las plataformas y Sistemas Operativos. Es un proyecto muy ambicioso que requiere de mucho trabajo. Sin embargo, creemos que beneficiará no sólo a los disrooters sino a toda la comunidad de Código Abierto que corre el mismo software o similar.

Para cubrir todos los aspectos de cada servicio y mantenerlos constantemente al día, necesitamos toda la ayuda de los disrooters. Así que si sientes que está faltando un tutorial, o que la información no es precisa, por favor, siéntete libre de contactarnos o escribir una revisión tú mismo. Como probablemente hayas notado, usamos **git** así que sólo haz una petición en nuestro repositorio. Si no estás familiarizado con git, puedes enviar cambios vía etherpad o email. Pronto generaremos un simple manual de git (o al menos un link a uno) para tener a toda la gente interesada a bordo.


![](en/disroot_logo.png)
