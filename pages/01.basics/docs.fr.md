---
title: Notions de base
published: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

# Bienvenue sur les pages "Comment faire" de **Disroot**.

Ce site est dédié à des guides et des tutoriels pour vous aider à trouver votre chemin dans les différents services Disroot.

Notre idée est de couvrir tous les services avec toutes les fonctionnalités fournies par disroot, pour toutes les plateformes et systèmes d'exploitation. C'est un projet très ambitieux qui demande beaucoup de travail. Cependant, nous pensons qu'il bénéficiera non seulement aux disrooters mais aussi à toute la communauté Open Source qui utilise des logiciels identiques ou similaires.

Afin de couvrir tous les aspects de chaque service et de le maintenir constamment à jour, nous avons besoin de l'aide de tous les disrooters. Donc, si vous avez l'impression qu'un tutoriel manque, ou que l'information n'est pas précise, n'hésitez pas à nous contacter ou écrivez une révision vous-même. Comme vous l'avez probablement remarqué, nous utilisons **git** donc il suffit de soumettre une demande de "pull" à notre dépôt.
Si vous n'êtes pas familier avec git, vous pouvez soumettre des modifications via etherpad ou par e-mail. Bientôt nous allons produire un simple git "Comment faire" sur git (ou au moins un lien vers un git) pour que toutes les personnes intéressées nous rejoignent.
![](en/disroot_logo.png)
