---
title: 'Come eliminare il tuo account da disroot'
published: true
visible: true
taxonomy:
    category:
        - docs

---

Per eliminare il tuo account dovrai solo completare il modulo di Disroot [eliminazione del modulo di richiesta conto](https://disroot.org/forms/delete-account-form)

Dopo aver confermato il tuo ordine, il tuo account verrà aggiunto alla nostra lista di eliminazione e verrà eliminato il primo lunedì dopo il periodo di attesa di una settimana.

Se si cambia idea durante il periodo di attesa e desidera mantenere il vostro account, vi preghiamo di contattarci via support@disroot.org
