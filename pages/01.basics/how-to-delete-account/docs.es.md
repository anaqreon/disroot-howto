---
title: 'Cómo borrar tu cuenta de disroot'
published: true
visible: true
taxonomy:
    category:
        - docs

---

Para borrar tu cuenta de Disroot sólo necesitas completar el [formulario de solicitud de borrado de la cuenta.](https://disroot.org/forms/delete-account-form)

Una vez que confirmas el pedido, tu cuenta será agregada a nuestra lista de eliminación y será borrada el primer lunes posterior al período de espera de una semana.

Si cambias de opinión durante el período de espera, y quieres mantener tu cuenta, por favor, contacta con nosotros vía support@disroot.org
