---
title: 'Hogyan töröld a Disroot fiókodat'
published: true
visible: true
taxonomy:
    category:
        - docs

---

A Disroot fiókod törléséhez csak a [fióktörlési űrlapot](https://disroot.org/forms/delete-account-form) kell kötöltened.

Amint megerősíted a kérelmet, a fiókod hozzá lesz adva az eltávolítandó fiókok listájához és egy hétnyi várakozási idő után következő hétfőn törlésre kerül.

Ha meggondolod magad a várakozási idő alatt és meg akarod tartani a fiókodat, kérlek, írj nekünk a support@disroot.org-ra!
