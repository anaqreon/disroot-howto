---
title: 'How to delete your disroot account'
published: true
visible: true
taxonomy:
    category:
        - docs

---

To delete your Disroot account you just need to fill in the [delete account request form](https://disroot.org/forms/delete-account-form)

Once you confirm the request, your account will be added to our deletion list and will be deleted on the first monday following a one week waiting period.

If you change your mind during the waiting period, and you want to keep your account, please contact us via support@disroot.org
