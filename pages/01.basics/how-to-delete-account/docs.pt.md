---
title: 'Como apagar a sua conta disroot'
visible: true
published: true
taxonomy:
    category:
        - docs
---

Para apgar a sua conta de utilizador no disroot apenas necessita de preencher [o formulário para pedir o apagar da sua conta](https://disroot.org/forms/delete-account-form)

Assim que confirmar o seu pedido, a sua conta será apagada na primeira Segunda-Feira após um período de espera de uma semana.

Se mudar de ideias durante o período de espera e queira manter a sua conta, por favor contacte-nos através enviando-nos um email para support@disroot.org
