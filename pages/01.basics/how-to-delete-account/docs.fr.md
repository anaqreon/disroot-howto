---
title: 'Comment supprimer votre compte disroot'
published: true
visible: true
taxonomy:
    category:
        - docs

---

Pour supprimer votre compte Disroot, il vous suffit de remplir le [formulaire de demande de suppression de compte](https://disroot.org/forms/delete-account-form)

Une fois la demande confirmée, votre compte sera ajouté à notre liste de suppression et sera supprimé le premier lundi suivant une période d'attente d'une semaine.

Si vous changez d'avis pendant la période d'attente et que vous souhaitez conserver votre compte, veuillez nous contacter via support@disroot.org
