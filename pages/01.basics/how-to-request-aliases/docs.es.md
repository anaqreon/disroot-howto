---
title: Cómo solicitar Alias de correo electrónico
published: true
visible: true
taxonomy:
    category:
        - docs

---

Los Alias están disponibles para aportantes habituales. Con "aportantes habituales" nos referimos a aquellos que nos "compran" por lo menos una taza de café al mes.
No estamos promoviendo el café, que es en realidad un ejemplo muy a la mano respecto de lo que es [explotación e inequidad](http://thesourcefilm.com/) (http://www.foodispower.org/coffee/). Pensamos que es una buena manera de dejar a la gente mensurar por sí mismos cuánto pueden dar.
Por favor, tómate el tiempo para considerar tu contribución. Si puedes 'comprarnos' una taza de café al mes en Río De Janeiro eso está bien, pero si puedes permitirte pagar un Café Doble Descafeinado Con Crema extra al mes, entonces puedes ayurdarnos en serio a mantener la plataforma Disroot corriendo y asegurar que esté disponible de manera gratuita para otras personas con menos recursos.

Encontramos esta [lista](https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee) de precios de tazas de café alrededor del mundo, podría no ser muy precisa, pero da un buen indicio de los direfentes costos.

Para solicitar alias necesitas completar este  [formulario](https://disroot.org/forms/alias-request-form).
