---
title: Az alapok
published: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

# Üdvözlünk a **Disroot** útmutatói oldalain!

Ez az oldal olyan útmutatók és oktatóanyagok helyéül szolgál, melyek segítenek kiismerni magadat a Disroot különféle szolgáltatásaival kapcsolatban.

Azt tervezzük, hogy a Disroot nyújtotta összes szolgáltatásról szót ejtünk, beleértve az összes létező platformot és operációs rendszert. Ez egy nagyon becsvágyó projekt, amely rengeteg munkát igényel. Azonban mi úgy hisszük, hogy nem csak a Disroot felhasználói hasznára fog válni, hanem a nyílt forrású szoftverek közösségének annak a részét, amely ugyanolyan vagy hasonló szoftvert futtat.

Ahhoz, hogy az összes szolgáltatás mindegyik szempontját mindig korszerű legyen, szükségünk van az összes Disrootfelhasználó segítségére. Tehát ha úgy érzed, hiányzik egy oktatóanyag vagy az ott szereplő információ nem pontos, kérünk, nyugodtan vedd fel velünk a kapcsolatot vagy írj egy revíziót magadtól! Amint bizonyára már észrevetted, a **git**et használjuk, úgyhogy csak küldj pull requestet a tárolónkba! Ha nem ismered a gitet, az etherpaden vagy emailen keresztül is elküldheted a változásaidat, valamint találhatsz egy git gyorstalpalót a következő oldalon (egyelőre angolul): https://howto.disroot.org/en/contribute/how-to-use-git.

![](en/disroot_logo.png)
