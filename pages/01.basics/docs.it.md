---
title: Le basi
published: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

# Benvenuti ai manuali di **Disroot**. 

Questo sito è dedicato a manuali e tutorial per guidare l'utente attraverso i vari servizi di Disroot.

La nostra idea è di coprire tutti i servizi forniti da Disroot, con le loro caratteristiche, per tutte le piattaforme e sistemi operativi. È un progetto molto ambizioso che richiede molto lavoro. Tuttavia, crediamo che si andrà a beneficio non solo il disrooters ma il codice comunitario che esegue il software stesso o simile.

Per coprire tutti gli aspetti di ogni servizio e tenerli costantemente aggiornati, abbiamo bisogno di tutto l'aiuto dalla disrooters. Così se si sente che manca un tutorial, o che le informazioni non sono esatte, per favore, non esitate a contattarci o a scrivere una recensione. Come probabilmente avete notato, utilizzare **git** così si ottiene solo una richiesta nel nostro archivio elettronico.Se non si ha familiarità con git, è possibile inviare modifiche etherpad o via e-mail. Presto genererà un semplice manuale di git (o almeno un link) a uno per tutte le persone interessate a bordo.




![](en/disroot_logo.png)
