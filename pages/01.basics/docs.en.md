---
title: Basics
published: true
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

# Welcome to **Disroot** Howto pages.

This site is dedicated to Howtos and tutorials to help you find your way around the various Disroot services.

Our idea is to cover all the services with all it's features provided by disroot, for all platforms and Operating System out there. It's very ambitious project that requires a lot of work. However we believe it will benefit not only disrooters but entire Open Source community that runs same or similar software.

In order to cover all the aspects of each service and constantly keep it up to date, we need help of all disrooters. So if you feel you are missing a tutorial, or that the information is not accurate- please feel free to
contact us or write a revision yourself. As you probably noticed, we use **git** so just submit pull request to our repository.
If you are not familiar with git, you can submit changes via etherpad or email. Soon we will produce a simple git howto on git (or at least link to one) to get all the interested people onboard.



![](en/disroot_logo.png)
