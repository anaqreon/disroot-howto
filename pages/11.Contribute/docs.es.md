---
title: Contribuir
published: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Esta sección está dedicada a ayudar a todas esas estupendas personas que decidieron ayudarnos con contribuciones a las traducciones o creando tutoriales.
Aquí podremos información básica y algunas pautas para hacer que esta colaboración resulte los más sencilla posible y sin complicaciones.


![](contribute.png)
