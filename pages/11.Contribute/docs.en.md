---
title: Contribute
published: true
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

This section is dedicated to help all those awesome people who decided to aid us with contributions to translations and creating tutorials.
Here we will put basic info and guidelines to make this collaboration as smooth and painless as possible.


![](contribute.png)
