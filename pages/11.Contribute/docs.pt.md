---
title: Contribuir
published: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Esta página é dedicada a ajudar todas estas pessoas incríveis que decidiram ajudar-nos contribuindo com traduções e criando tutoriais dos nossos serviços.
Aqui publicaremos informação básica e orientações para tornar esta colaboração o mais fácil e fluída possível.


![](contribute.png)
