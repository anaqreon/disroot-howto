---
title: Calendar
published: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

# What is it?
Well, it's pretty obvious what a Calendar is!

# How to see a calendar?
Click on the hamburger menu, and choose **Events** (or **Calendar**).

![Calendar_access](en/Calendar_access.png)

There, you have different options:

- **View:** how is the calendar displayed. It can be by month, by week or by day
- **Export Calendar:** you get a .ics file that you can save and import in an app or website that allows .ics file importation.
- **Import Calendar:** you can import a .ics file.
- **Arrows:** use them to move from month to month, day to day, etc.

# How to create an event?

![Calendar_create_event](en/Calendar_create_event.gif)

Click on **Create Event**.

You then have several options. At the very least, you need to provide:
- An Event title
- A Start date and time

But you have several other options, and even more if you click on **Advanced options**.

You can also, like always with Hubzilla, decide who is going to see your event by clicking on the **locker**.

**Submit** when you're done.

Your event now appears in you calendar:

![Calendar_event_view](en/Calendar_event_view.png)

If you click on the even, a window pops-up. You then get more details if provided by the event creator.
You can also edit (click on the **pencil**) or delete (click on the **trash**) your event if you're its creator.

![Calendar_event_view_click](en/Calendar_event_view_click.png)

Automatically, a post is created to inform your audience of the event. You can see it on your channel.

![Calendar_post](en/Calendar_post.png)

You can give a lot of information about your event, for example:

![Calendar_post_disroot](en/Calendar_post_disroot.png)

# How to inform that I want to attend an event?
People may want to inform you that they will participate into your meeting. To do so, they just click on the **calendar icon** in your event post, and then click on the right answer.

![Calendar_attendance](en/Calendar_attendance.png)

As you can see in the Disroot event post above, 6 people indicated that they would attend the event.
