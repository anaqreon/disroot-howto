---
title: Permissions
published: true
taxonomy:
    category:
        - docs
visible: true
page-toc:
    active: true
---

Permissions in Hubzilla are more complete than you may be used to. This allows us to define more fine graded relationships than the black and white "this person is my friend, so they can do everything" or "this person is not my friend, so they can't do anything" permissions you may find elsewhere. You can give almost type of permission (view a photo, write on your channel, comment a post, etc.) to anything (file, post, wiki, etc.) to almost any person or groups of person that you wish (those you specifically allow, anyone connected, etc.)

---
