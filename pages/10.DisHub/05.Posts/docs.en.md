---
title: Posts
published: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---
In this section we will see the different options you have when posting or commenting.  
It's recommended to understand the different [permission roles](../permissions) first, so the posts are delivered to those who are meant to.
