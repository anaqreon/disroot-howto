---
title: DisHub
published: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

# **DisHub: Disroot's Hubzilla instance** Howto pages.

[**Hubzilla**](https://project.hubzilla.org/page/hubzilla/hubzilla-project) is a powerful, free and open source software to create decentralized and federated web platforms which includes webpages tools, wikis, forums, cloud storage, social networks and more. Any attempt to accurately define Hubzilla may end up being inadequate.<br>
While shares similarity with other web services software, **Hubzilla** offers some very particular features that no others do, like full and precise permission system that gives you control about the privacy of the information and content you may share; nomadic identity (which means you own your online identity and can take it across the network, no matter the hubs, instance, servers); multiple profiles and channels; account cloning; among other features.

At **Disroot**, we think it’s an awesome piece of software and that everybody should try it because of its potential and the power it’s giving to decentralization and federation services. So we built **DisHub**, the **Disroot’s Hubzilla instance**. But, due to the amount of features, apps, services and user control settings, and because Hubzilla is beyond comparison, it may be kind of difficult until you fully get it. That’s why we made this Howto.

Think of Hubzilla as your new digital house: You have many rooms that you can arrange and decorate as you want, it’s your place to talk and share whatever you want with the people you want. And more than that, because you can even take your home wherever you want.

So, exercise your freedom, your imagination and welcome to DisHub.

*We can only show you the door. You're the one that has to walk through it...*



![](en/round_logo.png)
