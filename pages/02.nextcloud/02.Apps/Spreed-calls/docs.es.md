---
title: 'Llamadas con Spreed'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Desde Enero de 2017, hemos implementado llamadas con Spreed en nuestra nube. Es una aplicación muy sencilla para conferencias de audio/video que puedes utilizar para comunicarte, no sólo con otros usuarios de Disroot, sino con cualquiera que disponga de un ordenador conectado a internet y un navegador actualizado que soporte tecnología [WebRTC](https://es.wikipedia.org/wiki/WebRTC).

![](en/spreed_main.png)


La interfaz de la aplicación es muy simple. La barra lateral izquierda te muestra una lista de las salas que has creado.


Abajo, a la derecha de la ventana, verás tu avatar y algunas opciones donde puedes:

![](en/spreed_bottom.png)

 - silenciar/habilitar tu micrófono
 - prender/apagar tu cámara
 - habilitar/deshabilitar el modo pantalla completa

Eso es básicamente todo. Deberías estar listo para hacer tu nueva llamada.

##Creando llamadas
Para empezar a llamar, necesitas crear una sala. Haz click en **"Elegir persona"** para hacerlo. En la ventana emergente tienes una opción para invitar a una cuenta ya existente de Disroot, o crear una sala pública.
Verás que hay un nueva sala listada en la barra lateral izquierda.
Cuando haces click en el botón de las opciones de la sala, puedes:
  - invitar a más gente
  - crear/borrar un link público
  - abandonar la llamada/sala

![](en/spreed_create_calls1.png)

No hay verdadera diferencia entre las salas de usuarios de Disroot y las salas públicas, ya que en ambos casos puedes invitar a usuarios de Disroot o crear/eliminar links a salas públicas.

Una vez que inicias la llamada, presionando el nombre de la sala, una notificación será enviada al usuario de Disroot invitado. En el caso de una instancia de sala pública, tendrás que esperar que se una al menos un participante más.

Cuando se unan, pueden comenzar a tener su video llamada.
