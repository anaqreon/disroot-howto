---
title: 'Appeler avec Spreed'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Depuis janvier 2017, nous avons introduit des appels spreed dans notre cloud. Il s'agit d'une application de conférence audio/vidéo très simple que vous pouvez utiliser pour communiquer non seulement avec d'autres utilisateurs disroot, mais aussi avec toute personne qui a un ordinateur connecté à Internet et un navigateur web à jour et supportant la technologie WebRTC.

![](en/spreed_main.png)


L'interface de l'application est très simple. La barre latérale gauche affiche la liste des salles que vous avez créées.

En bas à droite de la fenêtre vous verrez votre avatar et quelques options où vous pouvez:

![](en/spreed_bottom.png)

- mettre en sourdine votre microphone
- allumer/éteindre votre appareil photo
- activer/désactiver le mode plein écran

C'est à peu près tout. Vous devriez être prêt à commencer un nouvel appel.

## Créer des appels
Pour commencer à appeler, vous devez créer une salle. Pour ce faire, cliquez sur **"Choisir la personne"**. Dans la fenêtre popup, vous avez une option pour inviter un compte existant ou créer une salle publique.
Vous verrez qu'il y a une nouvelle salle sur la barre latérale gauche.
En cliquant sur le bouton d'options de la salle, vous pouvez soit:
- inviter plus de gens
- créer/supprimer un lien public
- quitter l'appel/la salle

![](en/spreed_create_calls1.png)

Il n' y a pas de réelle différence entre les salles d'utilisateurs disroot et les salles publiques car dans les deux cas, vous pouvez inviter des utilisateurs disroot ou créer/supprimer des salles de liens publics.

Une fois que vous lancez l'appel en appuyant sur le nom de la salle, une notification sera envoyée à l'utilisateur invité ou (dans le cas d'une instance de salle publique) vous devrez attendre qu'au moins un autre participant se joigne à vous.

Une fois qu'ils s'inscrivent, vous pouvez commencer à recevoir votre appel vidéo.
