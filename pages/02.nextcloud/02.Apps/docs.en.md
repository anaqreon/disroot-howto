---
title: Nextcloud Apps
published: true
visible: true
taxonomy:
    category:
        - docs
---

In this section we are trying to cover all the apps we supply with out cloud instance.

**Note:**
This section covers only webapps. If you are looking for ways to integrate them with your device, you should check [this section](https://howto.disroot.org/nextcloud/sync-with-your-cloud)
