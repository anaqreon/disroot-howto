---
title: Aplicativos do Nextcloud
---

Nexta secção iremos tentar abordar todas as aplicações que fornecemos no nosso serviço de cloud.

**Nota:**
Esta secção apenas aborda aplicações web acessíveis via browser. Se está à procura de modos de integrar estas aplicações nos seus dispositivos sugerimos que consulte [esta secção.](https://howto.disroot.org/nextcloud/sync-with-your-cloud)
