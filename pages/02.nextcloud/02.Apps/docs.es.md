---
title: Aplicaciones Nextcloud
published: true
visible: true
taxonomy:
    category:
        - docs
---

En esta sección intentamos cubrir todas las aplicaciones que proveemos con nuestra instancia en la nube.

**Nota:**
Esta sección sólo cubre las aplicaciones web. Si estás buscando maneras de integrarlas con tu dispositivo, deberías ver en [esta sección](https://howto.disroot.org/nextcloud/sync-with-your-cloud)
