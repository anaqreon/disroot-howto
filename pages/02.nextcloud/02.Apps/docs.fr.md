---
title: Applications Nextcloud
published: true
visible: true
taxonomy:
    category:
        - docs
---

Dans cette section, nous essayons de couvrir toutes les applications que nous fournissons avec l'instance cloud.

**Note:**
Cette section ne couvre que les applications web. Si vous cherchez des façons de les intégrer à votre appareil, vous devriez voir [cette section](https://howto.disroot.org/nextcloud/sync-with-your-cloud)
