---
title: 'Webmail'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
  active: true
---
# Sincronizar os contactos da cloud com o webmail
Sincronizar os contactos da sua cloud com o webmail é bastante fácil. Permitirá os seus contactos estarem sincronizados entre o serviço de email e a cloud.

Primeiro vá à aplicação de contactos do Nextcloud. Carregue no botão "Definições", no canto inferior esquerdo.
Selecione "Copy Link" no livro de endereços que quer sincronizar com o seu email.

![](pt/webmail_import_contacts1.png)


Agora vá à aplicação de webmail e carregue no botão definições (no canto superior direito a aplicação de webmail)

![](pt/webmail_import_contacts2.png)

Nas suas definições, na barra lateral esquerda selecione: **Contactos**

Quando estiver no menu de contactos:

1. Selecione Ativar sincronização remota
2. Em "URL do Livro de endereços" coloque o link URL do livro de endereços que copiou anteriormente do Nextcloud.
3. Adicione o seu nome de utilizador
4. Adicione a sua password

![](pt/webmail_import_contacts3.png)

Depois recarregue ambas as páginas. A partir de agora os seus contactos estarão sincronizados.
