---
title: 'Webmail'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
  active: false
---
# Sincronizando los contactos en la nube con el webmail
Sincronizar los contactos en la nube con el webmail es muy sencillo y te permitirá tener ambas libretas actualizadas simultáneamente.

Primero, ve a tu aplicación de contactos de Nextcloud. Haz click en el ícono de configuraciones en la esquina inferior izquierda.
Selecciona la opción "Mostrar URL" de la libreta de direcciones que te gustaría sincronizar con el webmail, y copia el link.

![](en/webmail_import_contacts1.png)


Ahora, ve a la aplicación de Webmail, y haz click en el ícono de configuraciones (arriba a la derecha en la aplicación).

![](en/webmail_import_contacts2.png)

En tus configuraciones, en la barra lateral izquierda, selecciona: **Contactos**
Una vez dentro del menú de contactos:

1. Selecciona Habilitar sincronización remota
2. En URL de la Libreta de direcciones, pega la URL de tu libreta de contactos de Nextcloud que habías copiado anteriormente.
3. Introduce tu nombre de usuario
4. Agrega tu contraseña

![](en/webmail_import_contacts3.png)

Y luego actualiza ambas páginas. Ahora tus contactos permanecerán sincronizados.
