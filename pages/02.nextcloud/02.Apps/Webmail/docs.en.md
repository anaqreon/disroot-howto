---
title: 'Webmail'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
  active: false
---
# Syncing cloud contacts with webmail
Syncing cloud contacts with Webmail is very easy. It will permit contacts from your webmail and cloud to be in sync.

First go to your Nexcloud contacts app. Click on the settings icon in the lower left corner.
Select "Show URL" option of the addressbook you would like to sync with webmail, and copy the given link.

![](en/webmail_import_contacts1.png)


Now go to Webmail app, and click on the settings icon (top right in the webmail app)

![](en/webmail_import_contacts2.png)

In your settings, on the left side bar select: **Contacts**
When in the contacts menu:

1. Select Enable remote synchronization
2. In Addressbook URL, place the URL from your Nexcloud contacts addressbook you have saved before.
3. Provide your username
4. Add your password

![](en/webmail_import_contacts3.png)

And then refresh both pages. Now your contacts will stay in sync.
