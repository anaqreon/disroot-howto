---
title: 'Application News'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
  active: true
---

L'application News est un outil très pratique pour suivre dans un seul endroit les nouveaux articles et les messages de tous les sites web désirés. Le cloud Disroot fournit une application simple et géniale qui fait le travail pour vous et le synchronise avec tous vos périphériques. Dans ce tutoriel, nous allons essayer de couvrir l'utilisation de base, si vous êtes intéressé à l'intégrer avec vos appareils, choisissez votre système d'exploitation préféré dans [cette](https://howto.disroot.org/nextcloud/sync-with-your-cloud) section.


----------
# L'application News sur Disroot

Pour accéder au navigateur web du formulaire de l'application, cliquez simplement sur l'icône de l'application News ![](en/news_top_icon.png?resize=20,20) dans la barre supérieure. L'idée générale de l'application est très simple. Vous ajoutez le flux RSS de vos sites préférés, recevez des notifications sur les nouveaux articles, et lisez ensuite sans avoir besoin d'aller sur tous ces sites, en ouvrant beaucoup d'onglets et étant distrait à mi-chemin.

Alors, allons-y:

## Ajouter de nouveaux flux
La plupart des sites Web fournissent des fils [RSS](https://en.wikipedia.org/wiki/RSS) ou Atom. Une fois que le lien RSS est prêt, vous pouvez le copier et l'ajouter à votre application de nouvelles.
Pour ajouter le flux, cliquez simplement sur le bouton **"+ S'abonner"** et entrez l'URL du flux que vous avez copié précédemment.

![](en/news_add1.png)

Pour une meilleure vue d'ensemble et un meilleur tri, vous pouvez créer des dossiers et leur affecter des flux. Pour créer un nouveau dossier, appuyez sur le bouton **"+ Nouveau dossier"** dans la barre de gauche. Vous pouvez maintenant "glisser-déposer" les flux que vous souhaitez ajouter aux dossiers.

## Options des flux
Chaque flux peut être optimisé selon vos besoins. Cliquez sur l'icône **"Trois points"**:

![](en/news_options.png)
