---
title: 'Aplicación Contactos'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
  active: true
---

# Contactos
Puedes acceder a la aplicación Contactos presionando el ícono de contactos ![](en/contacts_top_icon.png?resize=20,20) en la barra superior en Nextcloud:

----------------------
## Crear un contacto

En la aplicación contactos selecciona "*Nuevo contacto*".

![](en/contacts_add1.png)

Aparecerá un formulario en la "*barra derecha*" para crear el nuevo contacto.

![](en/contacts_add2.png)

Escribe la información que quieras/tengas en los campos correspondientes. Si lo necesitas, puedes agregar más campos al final del formulario.

![](en/contacts_add3.png)

-----------------------
## Borrar un contacto

* Selecciona el contacto.
* En el encabezado del formulario de contacto, selecciona el ícono de borrar.

![](en/contacts_delete.png)

-----------------------
## Crear grupos de contactos
Puedes crear grupos para organizar tus contactos, por ejemplo: facultad, trabajo, colectivo, etc.
En el campo grupo puedes asignar un nuevo contacto a un grupo existente o crear un grupo nuevo. O asignar un contacto a múltiples grupos tipeando los varios grupos.

![](en/contacts_groups1.png)

A la izquierda de la pantalla en tu aplicación contactos, verás los grupos existentes. Al seleccionar uno presentará todos los contactos en ese grupo.

![](en/contacts_groups2.png)

------------------------
## Compartir la libreta de direcciones

Ve a "Configuraciones" en la esquina inferior izquierda de la pantalla en la aplicación de contactos.

![](en/contacts_share1.png)

En las configuraciones puedes compartir tu libreta de direcciones con otro usuario de Disroot seleccionando compartir libreta de direcciones y escribiendo el nombre de usuario de Disroot con el que deseas compartirla.

![](en/contacts_share2.png)

También puedes utilizar un link para compartir tu libreta de direcciones vía webDAV, a otras libretas (Thunderbird, móvil, etc,).

![](en/contacts_share3.png)

-------------------------
## Importar libreta de direcciones

Puedes importar libretas de direcciones o contactos individuales, si tienes un archivo vcf del contacto o la libreta.

* Selecciona "Importar".

![](en/contacts_import1.png)

Luego selecciona el archivo que quieres compartir, y presiona Ok.

-----------------------------
## Crear una nueva libreta de direcciones

Dentro de configuraciones, en el campo “Nombre de la libreta de direcciones” escribe el nombre de la nueva libreta, luego presiona enter.

![](en/contacts_create1.png)

-----------------------------
