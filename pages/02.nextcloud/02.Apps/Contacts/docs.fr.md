---
title: 'Application Contacts'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
  active: true
---

# Contacts
Vous pouvez accéder à l'application Contacts en appuyant sur l'icône Contacts ![](en/contacts_top_icon.png?resize=20,20) dans la barre supérieure, dans nexcloud.

----------------------
## Créer un contact

Dans l'application Contacts, sélectionnez "*Nouveau contact*".

![](en/contacts_add1.png)

Vous serez invité à utiliser un formulaire dans la "*barre de droite*" pour créer le nouveau contact.

![](en/contacts_add2.png)

Tapez simplement les informations que vous souhaitez/avez dans les champs. Si vous le souhaitez, vous pouvez ajouter d'autres champs au bas du formulaire.

![](en/contacts_add3.png)

-----------------------
## Supprimer un contact

* sélectionnez le contact
* dans l'en-tête du formulaire de contact, sélectionnez l'icône de suppression

![](en/contacts_delete.png)

-----------------------
## Créer des groupes de contact
Vous pouvez créer des groupes pour organiser vos contacts ex: faculté, travail, collectif, etc.
Dans le groupe de zones, vous pouvez affecter un nouveau contact à un groupe existant ou en créer un nouveau. Vous pouvez également affecter un contact à plusieurs groupes en tapant les différents groupes.

![](en/contacts_groups1.png)

Sur le côté gauche de l'écran dans l'application Contacts, vous verrez les groupes existants.
En les sélectionnant, tous les contacts de ce groupe seront présentés.

![](en/contacts_groups2.png)

------------------------
## Partager des carnets d'adresses

Allez dans "Paramètres" dans le coin inférieur gauche de l'écran, dans l'application Contacts.

![](en/contacts_share1.png)

Dans les paramètres, vous pouvez partager votre carnet d'adresses avec d'autres utilisateurs Disroot en:
sélectionnant le partage du carnet d'adresses
écrivant le nom d'utilisateur de l'utilisateur Disroot avec lequel vous souhaitez partager le carnet d'adresses.

![](en/contacts_share2.png)

Vous pouvez également utiliser un lien pour partager votre carnet d'adresses via webDAV, vers d'autres carnets de contacts (Thunderbird, mobile, etc.).

![](en/contacts_share3.png)

-------------------------
## Importer des carnets d'adresses

Vous pouvez importer des carnets d'adresses ou des contacts individuels, si vous avez un fichier vcf du contact ou du carnet d'adresses.

* sélectionnez "importer".

![](en/contacts_import1.png)

Sélectionnez ensuite le fichier que vous voulez importer et appuyez sur ok.

-----------------------------
## Créer un nouveau carnet d'adresses

A l'intérieur de "réglages", dans le champ "Nom du carnet d'adresses", inscrivez le nom du nouveau carnet d'adresses, puis appuyez sur Entrée.

![](en/contacts_create1.png)

-----------------------------
