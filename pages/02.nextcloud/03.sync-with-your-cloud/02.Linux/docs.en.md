---
title: 'Linux'
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

 Nextcloud integrates with Linux greatly. Below you can find useful links that will help you get everything up and running.

 ## Table of content
 - [Syncing files with desktop client](desktop-sync-client)
 - [GNOME - Desktop Integration](gnome-desktop-integration)
 - [KDE - Desktop Integration](kde-desktop-integration)
 - [News app - Syncing with your desktop](news-app-syncing)

 ![](Tux.png)
