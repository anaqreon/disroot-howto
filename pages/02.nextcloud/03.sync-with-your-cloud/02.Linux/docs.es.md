---
title: 'Linux'
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

 Nextcloud se integra con Linux enormemente. Debajo puedes encontrar links que te ayudrán a tener todo en funcionamiento.

 ## Tabla de Contenidos
 - [Sincronizando archivos con el cliente de escritorio](desktop-sync-client)
 - [GNOME - Integración con el escritorio](gnome-desktop-integration)
 - [KDE - Integración con el escritorio](kde-desktop-integration)
 - [News: Aplicación de Noticias - Sincronizando con tu escritorio](news-app-syncing)

 ![](Tux.png)
