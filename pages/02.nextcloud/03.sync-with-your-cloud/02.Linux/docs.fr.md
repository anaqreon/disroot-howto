---
title: 'Linux'
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

 Nextcloud s'intègre très bien à Linux. Vous trouverez ci-dessous des liens utiles qui vous aideront à tout mettre en place.

 ## Sommaire
 - [Synchroniser des fichiers avec votre client bureau](desktop-sync-client)
 - [GNOME - Intégration du Bureau](gnome-desktop-integration)
 - [KDE - Intégration du Bureau](kde-desktop-integration)
 - [Application News - Synchroniser avec votre Bureau](news-app-syncing)

 ![](Tux.png)
