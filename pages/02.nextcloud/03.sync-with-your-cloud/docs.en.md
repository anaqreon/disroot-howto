---
title: 'Syncing to your device'
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Nextcloud integrates with your device very easily, providing native experience for most devices and operating systems.
Here in disroot we are trying to include and document all of them to make it easy for anyone to get up and running.

If you would like to help us, your contribution is more then welcome.
