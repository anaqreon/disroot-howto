---
title: MacOS
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Below you can learn how to integrate Nextcloud with your MacOS device
- [Syncing Calendars](calendar-syncing)
- [Syncing Contacts](contact-syncing)

![](macos.jpg)
