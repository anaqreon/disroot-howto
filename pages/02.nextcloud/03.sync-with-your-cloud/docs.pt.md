---
title: 'Sincronizar com o seu dispositivo'
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

O Nextcloud pode ser integrado com bastante facilidade nos seus dispositivos (computador, telefone)com muita facilidade, fornecendo uma experiencia nativa para a maioria dos sistemas operativos e dispositivos.
Aqui no Disroot estamos a tentar incluir informação sobre todos os sistemas operativos para tornar mais fácil para todas as pessoas configurarem os seus serviços e aplicações do Disroot.

Se tiver interesse em ajudar-nos, a sua ajuda é mais que bem vinda.
