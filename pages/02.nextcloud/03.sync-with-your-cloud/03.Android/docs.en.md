---
title: Android
visible: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Below you can learn how to integrate nextcloud with your Android device.

- [Nextcloud app](Nextcloud-app)
- [Syncing Calendars, Contacts and Tasks](calendars-contacts-and-tasks)
- [Migrating contacts from Google into Nextcloud](https://howto.disroot.org/en/nextcloud/sync-with-your-cloud/android/migrating-contacts-from-google)
- [Syncing News](using-news)
- [Syncing Notes](Using-notes)

![](android.jpg)
