---
title: Android
visible: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Ci-dessous vous pouvez apprendre comment intégrer nextcloud avec votre appareil Android.

- [Application Nextcloud](Nextcloud-app)
- [Synchronisation des calendriers, contacts et tâches](calendars-contacts-and-tasks)
- [Synchronisation des actualités](using-news)
- [Synchronisation des notes](Using-notes)
![](android.jpg)
