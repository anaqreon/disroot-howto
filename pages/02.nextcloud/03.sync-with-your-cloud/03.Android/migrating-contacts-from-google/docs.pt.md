---
title: Migrar contactos
visible: true
---

### Mover os seus contactos da sua conta Google para a sua conta Disroot
*(Este tuturial é baseado no cyanogenmod 12.1 pequenas alterações podem ser necessárias noutras roms.)*

- Abra a aplicação de contactos
- Selecione Importar/Exportar a partir do menu no canto superior direito
- Selecione Exportar para armazenamento interno e selecione os contactos a exportar. (Existe a opção selecionar tudo). Isto irá criar um ficheiro VCF com os os contactos que selecionou no armazenamento interno do seu telemóvel)
- A partir do mesmo painel de Importar/Exportar selecione Importar a partir do Armazenamento.
- Selecione conta DAVdroid a partir da caixa criar conta. Isto irá adicionar contactos a partir do ficheiro VCF exportado. (Você também pode importar VCF's diretamente da sua conta nextcloud a partir do browser)

### Definir a sua conta Disroot como a conta principal para contactos telefónicos

- Vá a contactos => Contactos a mostrar => selecione DAVdroid
- Isto seleciona a sua conta Disroot como a conta onde armazenar novos contactos

Caso salte este passo lembre-se de selecionar o DAVdroid em vez da sua conta Google ou conta local cada vez que grava um novo contacto.

### Para de sincronizar contacto com o Goolge

- Vá a gestor de aplicações -> Todas
- Google App -> Permissões -> Contactos
- Isto irá desabilitar a sincronização de contactos com a Google até serem habilitados novamente
