---
title: 'Sincronizando tu dispositivo'
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Nextcloud se integra con tu dispositivo de manera muy sencilla, proporcionando una experiencia nativa para la mayoría de los dispositivos y sistemas operativos.
Aquí en Disroot intentamos incluir y documentarlos a todos para que sea sencillo para cualquiera ponerlos en marcha.

Si quisieras ayudarnos, tu contribución es más que bienvenida.
