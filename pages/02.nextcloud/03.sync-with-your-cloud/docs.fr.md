---
title: 'Synchroniser vers votre appareil'
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Nextcloud s'intègre très facilement avec votre appareil, offrant une expérience native pour la plupart des appareils et systèmes d'exploitation.
Ici, nous essayons d'inclure et de documenter tous ces éléments pour faciliter la mise en place et le fonctionnement de n'importe qui.

Si vous souhaitez nous aider, votre contribution est la bienvenue.
