---
title: Multiplatforma
published: true
visible: true
taxonomy:
    category:
        - docs
---

Aqui estão tutoriais para aplicações multiplataforma (apps que funcionam em sistema operativos diferentes).

- [Thunderbird - Sincronizar contatos, Calendários e Tarefas](thunderbird-calendar-contacts)

![](thunderbird.png)
