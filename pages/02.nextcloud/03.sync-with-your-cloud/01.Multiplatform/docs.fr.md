---
title: Multiplatforme
published: true
visible: true
taxonomy:
    category:
        - docs
---

Voici comment ajouter des guides et des tutoriels pour les applications multiplateformes (applications qui fonctionnent sur différents systèmes d'exploitation).

- [Synchronisation de Thunderbird - Calendrier et Contact](thunderbird-calendar-contacts)
- [Calcurse calendar sync](calcurse-caldav)
