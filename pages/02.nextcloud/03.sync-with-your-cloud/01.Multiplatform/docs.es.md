---
title: Multiplatforma
published: true
visible: true
taxonomy:
    category:
        - docs
---

Aquí están agregados manuales y tutoriales para aplicaciones multiplatforma (aquellas que funcionan en diferentes sistemas operativos).

- [Thunderbird: Sincronización de Calendario y Contactos](thunderbird-calendar-contacts)
- [Calcurse calendar sync](calcurse-caldav)
