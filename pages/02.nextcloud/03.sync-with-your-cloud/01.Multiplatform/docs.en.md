---
title: Multiplatform
published: true
visible: true
taxonomy:
    category:
        - docs
---

Here are howtos and tutorials for multiplatform applications (apps that work on different operating systems).

- [Thunderbird - Calendar and Contact sync](thunderbird-calendar-contacts)
- [Calcurse calendar sync](calcurse-caldav)
